import { forEach, filter } from 'lodash'

export const checkValidation = (fields, locale) => {

    // Check if all fields are validated
    
    let fieldsValid = true;
    
    forEach(fields, (item) => {
        const valid = item.validator ? item.validator(item.value, fields) : item.valid;

        if (!valid) {
            fieldsValid = false
        }     
    })

    return fieldsValid
}	

export const getPosForElement = (element) => {
    if (!element) return;

    const offset = 45;
    const bodyRect = document.body.getBoundingClientRect().top;
    const elementRect = element.getBoundingClientRect().top;
    const elementPosition = elementRect - bodyRect;
    const offsetPosition = elementPosition - offset;
    return offsetPosition
}

export const getIntitalState = (data, useSections) => {
    let fields = {}

    // Sections

    if (useSections) {
        forEach(data.sections, (section) => {
            forEach(section.fields, (field) => {
                fields[field.name] = {
                    name: field.name,
                    value: field.defaultValue || undefined,
                    valid: field.validator ? field.validator('') : true,
                    validator: field.validator
                }
            })
        })
    } 

    // Fields

    if (!useSections) {
        forEach(data.fields || data, (field) => {
            fields[field.name] = {
                name: field.name,
                value: field.defaultValue || null,
                valid: field.validator ? field.validator('') : true,
                validator: field.validator
            }
        })
    }
  
    return fields;
}

export const getFieldProps = (i, data, validate, disabled, settings, fieldState) => {
    return {
        key: i,
        id: data.name,
        type: data.type,
        label: data.label,
        placeholder: data.placeholder, 
        value: fieldState ? fieldState.value : data.defaultValue || '',
        validator: data.validator,
        isValid: fieldState ? fieldState.valid : false, 
        shouldValidate: validate ? validate : fieldState && fieldState.value,
        errorMessage: data.errorMessage || 'This is a required field',
        helperText: data.helperText,
        disabled: disabled,
        component: data.component,
    };
}

// Error checking

export const isDataValid = (props) => {
    const { useSections, data } = props
    const { sections, fields } = data
    const prefix = '(React Dynamic Form) Warning:'

    if (useSections) {
        if (!sections) return console.error(`${prefix} No 'sections' object supplied`)
    } 

    if (!useSections) {
        if (!fields && !data.length) return console.error(`${prefix} No 'fields' object supplied`)
    }

    return true
}

// Populate fields

export const populateFields = (data, stateFields) => {

    // Loop over passed field data and map to stateFields

    forEach(data, (value, key) => {
        const match = stateFields[key]

        if (match) {
            stateFields[key] = {
                ...match,
                value: value,
                valid: match.validator ? match.validator(value) : true
            }
        }
    })

    return stateFields
}