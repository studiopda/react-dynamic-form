import React from "react";
import { getFieldProps } from "./utils";

import Input from "./components/Input";

const exampleField = {
    type: 'email',
    name: 'email',
    label: 'Email Address',
    validator: () => { return false },
    errorMessage: 'Please enter a valid email'
}

const customComponentField = {
    name: 'countrySelect',
    label: 'Country select',
    component: ({value, onChangeValue, validator}) => {
        return (
            <select/>
        )
    }
}

const defaultProps = {
    onSubmit: () => { 
        return true 
    },
    moveToInvalidField: true,
    getFieldProps: getFieldProps,
    data: {
        // Sections example

        sections: [
            {
                fields: [
                    {...exampleField},
                    {...customComponentField}
                ]
            },
        ],

        // Fields (no sections)

        fields: [
            {...exampleField},
            {...customComponentField}
        ]
    }
}

export default defaultProps 