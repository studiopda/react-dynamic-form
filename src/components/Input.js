import React, {Component} from 'react'
import styled, { css } from 'styled-components'

class Input extends Component {

    state = {
        focused: false
    }

    onBlur = () => {
        const { toggleFocus } = this.props;

        this.setState({ focused: false })

        if (toggleFocus) {
            toggleFocus(false)
        }
    }

    onFocus = () => {
        const { toggleFocus } = this.props;

        this.setState({ focused: true })

        if (toggleFocus) {
            toggleFocus(true)
        }
    }

    shouldComponentUpdate() {
        // Stop stripe fields from updating

        if (this.props.stripeOptions) {
            return false
        }

        return true
    }
    
    onChangeValue = (e) => {
        const { validator, onChangeValue } = this.props;
        const value = e.target.value;
        
        if (onChangeValue) {
            onChangeValue(value, validator)
        }
    }

    shouldShowError = () => {
        const { focused } = this.state;
        const { shouldValidate, isValid } = this.props;

        if (shouldValidate === true && !isValid) {
            return true
        }

        if (shouldValidate && !isValid && !focused) {
            return true
        }
    }

    getInputProps = () => {
        const { value, label, type, placeholder, className, disabled, name, locale, id, icon} = this.props;
        const showError = this.shouldShowError();

        return {
            className: className,
            id: name,
            type: type,
            placeholder: placeholder,
            value: value,
            disabled: disabled,
            hasIcon: icon,
            onChange: !disabled && this.onChangeValue,
            error: showError,
            onFocus: this.onFocus,
            onBlur: this.onBlur,
        }
    }
    

    render() {
        const { focused } = this.state;
        const { errorMessage, helperText, id, type, label, validator, component } = this.props;
        const showError = this.shouldShowError();
        const CustomComponent = component || false;
        
        return (
            <Wrapper
                id={id}
                className={[id, 'dynamic-field']}
            >
                <Field
                    error={showError}
                >
                    {!CustomComponent && (
                        <InputField 
                            {...this.getInputProps()}
                        />
                    )}

                    {CustomComponent && (
                        <CustomComponent
                            {...this.props}
                        />
                    )}
                </Field>

                {showError && (
                    <Error>
                        {errorMessage}
                    </Error>
                )}

            </Wrapper>
        )
    }
}

export const red = '#CB0000';

export const Wrapper = styled.div`
    position: relative;
    width: 100%;
`

// Field


export const inputStyle = css`
    appearance: none;
    box-shadow: none;
    display: flex;
    flex: 1;
    height: 30px;
    width: 100%;
    box-sizing: border-box;
    border: 0;
    border: 1px solid black;
    box-sizing: border-box;
    background: transparent;
    padding: 0 10px;

    &, &::placeholder {
        font-size: 14px;
        font-family: inherit;
        color: black;
    }

    &::placeholder {
        color: rgba(0, 0, 0, 0.5);
    }

    &:focus {
        outline: none;
    }

    ${props => {
        if (props.error) return css`
            & {
                color: ${red} !important;
            }

            &::placeholder {
                color: black;
            }

            &, &:focus {
                border: 1px solid ${red};
            }
        `
    }}

    ${props => {
        if (props.disabled) return css`
            pointer-events: none;
            
            &, &::placeholder {
                color: rgba(0, 0, 0, 0.2);
            }
        `
    }}
`

const Field = styled.div`
    display: flex;
    width: 100%;
`

export const InputField = styled.input`
    ${inputStyle}
`

// Helper Text

const message = css`
    line-height: 16px;
    font-size: 13px;
    letter-spacing: -0.01px;
`

// Error

export const Error = styled.div`
    ${message}
    color: ${red} !important; 
    margin-top: 8px;
	margin-left: auto;
`

export default Input;