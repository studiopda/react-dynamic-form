import React, { Component, cloneElement } from "react";
import styled, { css, injectGlobal } from "styled-components";
import { find, capitalize, findIndex, debounce, forEach, xor } from "lodash";

import defaultProps from './defaultProps'
import { getIntitalState, getFieldProps, checkValidation, getPosForElement, isDataValid, populateFields } from "./utils";
import Input from "./components/Input";

class DynamicForm extends Component {

	static defaultProps = {
		...defaultProps
	}

	state = {
		validate: false,
		fields: {},
		submitting: false,
        stripeToken: null,
        showServerErrors: false,
		serverErrorMessage: '',
		shippingPrice: null,
		calculatingShipping: false,
		globalValidation: {
			error: false,
			message: null
		}
	}

	componentWillMount = () => {
		this.initialiseForm()
	}

	initialiseForm = () => {
		const { useSections, data, populatedFields } = this.props;
		const dataValid = isDataValid(this.props);

		// Generate initial state for field data
		        
		if (dataValid) {
			this.setState({
				fields: getIntitalState(data, useSections),  
			}, () => {
				if (populatedFields) this.populateFields()
			})
		}
	}

	handleFieldChange = (key, value, validator) => {
		const { fields } = this.state;
		const { onFieldChange } = this.props;

		const fieldData = {
			name: key,
			value: value,
			valid: validator ? validator(value, fields) : true
		}
		
        this.setState({
            fields: {
				...fields,
				[key]: {
					...fieldData
				}
			}
		})
		
		// onFieldChange call back

		if (onFieldChange) onFieldChange(fieldData, fields)
	}
	
	componentDidUpdate(prevProps, prevState) {
		const { populatedFields, data } = this.props;
		const formChanged = data.id && data.id !== prevProps.data.id;

		// Re-intialise if new form data supplied
		
		if (formChanged) {
			this.initialiseForm()
		}

		// Populate fields if supplied post mount

		if (!formChanged && !prevProps.populatedFields && populatedFields) {
			this.populateFields()
		}

		// Detect if populatedFields structure has changed and repopulate

		if (!formChanged && xor(prevProps.populatedFields, populatedFields).length !== 0) {
			this.populateFields()
		}
	}

	populateFields = () => {
		const populatedFields = populateFields(this.props.populatedFields, this.state.fields);
		this.setState({
			fields: {
				...this.state.fields,
				...populatedFields
			}
		})
	}
	
	handleSubmit = (params) => {
		const { fields, submitting } = this.state;
		const { cart, onSubmit } = this.props;

		// Show valiation errors

		this.setState({ validate: true })

		// Submit if all fields valid

		if (!submitting && this.checkValidation()) {

			this.setState({ 
				submitting: true,
				globalValidation: {
					error: false,
					message: null
				}
			})
			
			onSubmit(fields, (error) => {
				this.setState({ 
					submitting: false,
				})
			})	
		}
	}
	
	checkValidation = () => {
		const { fields, shippingPrice } = this.state
		const { settings, moveToInvalidField, onValidationError } = this.props;
		let hasError = false;	

		// Scroll to error

		const firstError = find(fields, (item) => {
			const valid = item.validator ? item.validator(item.value, fields) : item.valid;

			if (!valid) {
				return true
			}    
		})	

		if (moveToInvalidField && firstError && firstError.name) {
			const element = document.getElementById(firstError.name);

			window.scrollTo({
				top: getPosForElement(element),
				behavior: "smooth"
			});
		}

		// onValidationError callback

		onValidationError && onValidationError(firstError)
		return checkValidation(fields)
	}

	renderSubmit = () => {
		const { renderSubmit } = this.props;
		const { submitting } = this.state;

		if (renderSubmit == false) return;
		if (renderSubmit) return renderSubmit(this.handleSubmit, submitting)

		return (
			<Submit
				onClick={this.handleSubmit}
			>
				Submit
			</Submit>
		)
	}


	renderField = (i, data, validate, disabled) => {
		const { settings, getFieldProps, renderInput } = this.props;
		const { fields } = this.state;
		const fieldState = fields[data.name];
		
		const props = {
			onChangeValue: (value, validator) => this.handleFieldChange(data.name, value, validator),
			...getFieldProps(i, data, validate, disabled, settings, fieldState),
			...fieldState,
			fieldState: this.state.fields
		}

		// Return if field should be hidden

		if (data.hide && data.hide(fields)) return;

		// Render input w/ props

		return (
			cloneElement(renderInput || <Input/>, {
				...props,
			}) 
		)
	}

	renderSections = () => {
		const { fields } = this.state;
		const { data, renderSection } = this.props;
	
		return data.sections.map((section, i) => {
			if (section.hide && section.hide(fields)) return;
			const sectionFields = this.renderFields(section.fields);

			// Render custom section if passed

			if (renderSection) return renderSection(section, sectionFields, i)

			// Else render default
			
			return (
				<Section
					key={i}
				>
					{fields}
				</Section>
			)
		})		
	}
	
	renderFields = (fields) => {
		const { validate } = this.state;

		return (
			<Fields
				className={'dynamic-fields'}
			>
				{fields.map((field, i) => {
					return this.renderField(i, field, validate)
				})}
			</Fields>
		)
	}
    
    renderServerErrors = () => {
		const { showServerErrors, serverErrorMessage } = this.state;
		if (!showServerErrors) return 
		
        return (
            <Error>{serverErrorMessage}</Error>
        )  
    }

  	render() {	
		const { useSections, data, styles} = this.props;
		const dataValid = isDataValid(this.props);

		return (
			<Wrapper
				styles={styles}
			> 
				{dataValid && (
					<>
						{(useSections ? this.renderSections() : this.renderFields(data.fields || data))}
						{this.renderSubmit()}
					</>
				)}
			</Wrapper> 
		)
	}	 
}

const Wrapper = styled.div`
	${props => props.styles}
`

// Shared

const Heading = styled.div``
const Subheading = styled.div``
const Description = styled.div``
const Info = styled.div``
const Line = styled.div``

const Error = styled.div`
	line-height: 16px;
	font-size: 13px;
	letter-spacing: -0.01px;
    margin-top: 16px;
    color: red;
	text-align: center;
`


// Form Sections

const Section = styled.div`
	display: flex;
    flex-direction: column;

	&:not(:first-child) {
		margin-top: 36px;
	}
`

export const fieldsWrapper = css`
	display: flex;
	flex-flow: row wrap;

	&:not(:first-child) {
		margin-top: 32px;
	}
`

const Fields = styled.div`
	${fieldsWrapper}
`

// Submit

const Submit = styled.div`
	height: 40px;
	width: 100%;
	background: black;
	color: white;
	cursor: pointer;
	font-weight: bold;
	
	display: flex;
	justify-content: center;
	align-items: center;
`

export default DynamicForm